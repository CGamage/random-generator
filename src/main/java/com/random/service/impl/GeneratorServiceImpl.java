/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.service.impl
 */
package com.random.service.impl;

import java.util.UUID;

import org.springframework.stereotype.Service;

import com.random.service.GeneratorService;

/**
 * @author Tharaka Chandralal
 */
@Service
public class GeneratorServiceImpl implements GeneratorService{

	/* (non-Javadoc)
	 * @see com.random.service.GeneratorService#uuidKey()
	 */
	@Override
	public String uuidKey() throws Exception {
		// TODO Auto-generated method stub
		return UUID.randomUUID().toString();
	}

}
