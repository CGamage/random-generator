/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.service.impl
 */
package com.random.service.impl;

import java.util.Base64;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.random.dto.LoginDto;
import com.random.entity.Register;
import com.random.repository.RegisterRepository;
import com.random.service.LoginService;

/**
 * @author Tharaka Chandralal
 */
@Service
public class LoginServiceImpl implements LoginService{

	@Autowired
	private RegisterRepository registorRepository;
	
	/* (non-Javadoc)
	 * @see com.random.service.LoginService#login(com.random.dto.LoginDto)
	 */
	@Override
	public String login(LoginDto loginDto) throws Exception {
		
		if (loginDto.getEmployeeEmail() !=null && loginDto.getPassword() !=null) {
			
			String encode = Base64.getEncoder().encodeToString(loginDto.getPassword().getBytes());
			Register register = registorRepository.findByEmailAndPassword(loginDto.getEmployeeEmail(),encode);
			
			if (register != null) {
				return "Authorized . . !";
			}else {
				return "Un Authorized . . !";
			}
		}else {
			return "Enter Email And Password . . !";
		}
		
	}

}
