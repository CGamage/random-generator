/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.service.impl
 */
package com.random.service.impl;

import java.util.Base64;
import java.util.List;
import java.util.UUID;


import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.random.dto.RegisterDto;
import com.random.entity.Register;
import com.random.repository.RegisterRepository;
import com.random.service.RegisterService;
import com.random.utill.AppConstant;

/**
 * @author Tharaka Chandralal
 */
@Service
public class RegisterServiceImpl implements RegisterService{

	@Autowired
	private RegisterRepository registerRepository;
	
	/* (non-Javadoc)
	 * @see com.random.service.RegisterService#saveEmployee(com.random.dto.RegisterDto)
	 */
	@Override
	public String saveEmployee(RegisterDto registerDto) throws Exception {
		
		Register reg = registerRepository.findByEmail(registerDto.getEmail());

		if (reg != null) {
			return "Employee All Ready Exist . . !";
		}else {
			Register register = new  Register();
			
			register.setRegistorId(UUID.randomUUID().toString());
			register.setEmail(registerDto.getEmail());
			register.setGender(registerDto.getGender());
			register.setMobileNumber(registerDto.getMobileNumber());
			register.setName(registerDto.getName());
			register.setNic(registerDto.getNic());
			register.setPassword(Base64.getEncoder().encodeToString(registerDto.getPassword().getBytes()));
			register.setStatus(AppConstant.ACTIVE);
			
			if (registerDto.getPassword().equals(registerDto.getConfirmPassword())) {
				registerRepository.save(register);
				return "Employee Save Succsess . . !";
			}else {
				return "Employee Save Faild . . !";
			}
		}
	}

	/* (non-Javadoc)
	 * @see com.random.service.RegisterService#updateEmployee(com.random.dto.RegisterDto)
	 */
	@Override
	public String updateEmployee(RegisterDto registerDto) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.random.service.RegisterService#deleteEmployee(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public String deleteEmployee(String userName, String email, String nic) throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	/* (non-Javadoc)
	 * @see com.random.service.RegisterService#searchEmployee(java.lang.String, java.lang.String, java.lang.String)
	 */
	@Override
	public RegisterDto searchEmployee(String userName, String email, String nic) throws Exception {
		
		Register register = registerRepository.findByEmailOrNicOrName(email, nic, userName);
		
		RegisterDto registerDto = getRegistor(register);
		return registerDto;
	}

	/* (non-Javadoc)
	 * @see com.random.service.RegisterService#getAllEmployee()
	 */
	@Override
	public List<RegisterDto> getAllEmployee() throws Exception {
		// TODO Auto-generated method stub
		return null;
	}

	private RegisterDto getRegistor(Register register)throws Exception{
		
		RegisterDto registerDto = new RegisterDto();
		registerDto.setRegistorId(register.getRegistorId());
		registerDto.setName(register.getName());
		registerDto.setEmail(register.getEmail());
		registerDto.setGender(register.getGender());
		registerDto.setMobileNumber(register.getMobileNumber());
		registerDto.setNic(register.getNic());
		byte[] byt= Base64.getDecoder().decode(register.getPassword());
		String decode = new String(byt);
		registerDto.setPassword(decode);
		
		return registerDto;
	}
}
