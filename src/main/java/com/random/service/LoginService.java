/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.service
 */
package com.random.service;

import com.random.dto.LoginDto;

/**
 * @author Tharaka Chandralal
 */
public interface LoginService {

	public String login(LoginDto loginDto)throws Exception;
}
