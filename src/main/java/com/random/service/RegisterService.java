/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.service
 */
package com.random.service;

import java.util.List;

import com.random.dto.RegisterDto;

/**
 * @author Tharaka Chandralal
 */
public interface RegisterService {

	public String saveEmployee(RegisterDto registerDto)throws Exception;
	
	public String updateEmployee(RegisterDto registerDto)throws Exception;
	
	public String deleteEmployee(String userName,String email,String nic)throws Exception;
	
	public RegisterDto searchEmployee(String userName,String email,String nic)throws Exception;
	
	List<RegisterDto>getAllEmployee()throws Exception;
}
