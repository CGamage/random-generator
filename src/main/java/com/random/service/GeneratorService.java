/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.service
 */
package com.random.service;

/**
 * @author Tharaka Chandralal
 */
public interface GeneratorService {

	public String uuidKey()throws Exception;
}
