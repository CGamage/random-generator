/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.repository
 */
package com.random.repository;

import org.springframework.data.jpa.repository.JpaRepository;

import com.random.entity.Register;

/**
 * @author Tharaka Chandralal
 */
public interface RegisterRepository extends JpaRepository<Register, String>{

	Register findByEmail(String email);
	Register findByNic(String nic);
	Register findByName(String name);
	Register findByEmailAndNicAndName(String email,String nic,String name);
	Register findByEmailOrNicOrName(String email,String nic,String name);
	Register findByEmailAndPassword(String email,String password);
	
}
