/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.dto
 */
package com.random.dto;

/**
 * @author Tharaka Chandralal
 */
public class RegisterDto {

	private String registorId;
	private String name;
	private String email;
	private String nic;
	private String gender;
	private String password;
	private String confirmPassword;
	private String mobileNumber;
	
	public RegisterDto() {
		
	}

	public String getRegistorId() {
		return registorId;
	}

	public void setRegistorId(String registorId) {
		this.registorId = registorId;
	}

	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	public String getConfirmPassword() {
		return confirmPassword;
	}

	public void setConfirmPassword(String confirmPassword) {
		this.confirmPassword = confirmPassword;
	}

	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}
	
	
}
