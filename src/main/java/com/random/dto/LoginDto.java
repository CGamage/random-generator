/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.dto
 */
package com.random.dto;

/**
 * @author Tharaka Chandralal
 */
public class LoginDto {

	private String employeeEmail;
	private String password;
	
	public LoginDto() {
		
	}

	
	public String getEmployeeEmail() {
		return employeeEmail;
	}


	public void setEmployeeEmail(String employeeEmail) {
		this.employeeEmail = employeeEmail;
	}


	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}
	
	
}
