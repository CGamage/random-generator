/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.controller
 */
package com.random.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.random.dto.LoginDto;
import com.random.service.LoginService;

/**
 * @author Tharaka Chandralal
 */
@RestController
@CrossOrigin
@RequestMapping(value="/random/login")
public class LoginController {

	@Autowired
	private LoginService loginService;
	
	@PostMapping(value="/sigin")
	public ResponseEntity<Object>login(@RequestBody LoginDto loginDto){
		try {
			return new ResponseEntity<Object>(loginService.login(loginDto),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
