/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.controller
 */
package com.random.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.random.service.GeneratorService;

/**
 * @author Tharaka Chandralal
 */
@RestController
@CrossOrigin
@RequestMapping(value="/random/generator")
public class GeneratorController {

	@Autowired
	private GeneratorService generatorService;
	
	@GetMapping(value="/getRandomKey")
	public ResponseEntity<String>uuidKey(){
		try {
			return new ResponseEntity<String>(generatorService.uuidKey(),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<String>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
