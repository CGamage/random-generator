/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.controller
 */
package com.random.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import com.random.dto.RegisterDto;
import com.random.service.RegisterService;

/**
 * @author Tharaka Chandralal
 */
@RestController
@CrossOrigin
@RequestMapping(value="/Rendom/Registor")
public class RegisterController {

	@Autowired
	private RegisterService registerService;
	
	@PostMapping(value="/saveEmployee")
	public ResponseEntity<Object>saveEmployee(@RequestBody RegisterDto registerDto){
		try {
			return new ResponseEntity<Object>(registerService.saveEmployee(registerDto),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
	
	@GetMapping(value="/searchEmployee/{userName:.+}/{email}/{nic}")
	public ResponseEntity<Object>searchEmployee(@PathVariable("userName")String userName,@PathVariable("email")String email,@PathVariable("nic")String nic){
		try {
			return new ResponseEntity<Object>(registerService.searchEmployee(userName, email, nic),HttpStatus.OK);
		} catch (Exception e) {
			e.printStackTrace();
			return new ResponseEntity<Object>(e.getMessage(),HttpStatus.INTERNAL_SERVER_ERROR);
		}
	}
}
