/**
 * Jun 26, 2019	
 * Random-Generator
 * com.random.entity
 */
package com.random.entity;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.Id;
import javax.persistence.Table;

/**
 * @author Tharaka Chandralal
 */
@Entity
@Table(name="REGISTOR")
public class Register {

	private String registorId;
	private String name;
	private String email;
	private String nic;
	private String gender;
	private String password;
	private String mobileNumber;
	private String status;
	
	public Register() {
		
	}

	@Id
	@Column(name="PID",nullable=false)
	public String getRegistorId() {
		return registorId;
	}

	public void setRegistorId(String registorId) {
		this.registorId = registorId;
	}

	@Column(name="NAME",nullable=false)
	public String getName() {
		return name;
	}

	public void setName(String name) {
		this.name = name;
	}

	@Column(name="EMAIL",nullable=true)
	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	@Column(name="NIC",nullable=true)
	public String getNic() {
		return nic;
	}

	public void setNic(String nic) {
		this.nic = nic;
	}

	@Column(name="GENDER",nullable=false)
	public String getGender() {
		return gender;
	}

	public void setGender(String gender) {
		this.gender = gender;
	}

	@Column(name="PASSWORD",nullable=false)
	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Column(name="MOBILE_NUMBER",nullable=true)
	public String getMobileNumber() {
		return mobileNumber;
	}

	public void setMobileNumber(String mobileNumber) {
		this.mobileNumber = mobileNumber;
	}

	@Column(name="STATUS",nullable=false)
	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}
	
	
	
	
}
